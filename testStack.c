// testStack.c by *****

#include <stdio.h>
#include "stack.h"
#include "testCommon.h"
#include "math.h"

void testInitStack() {
    stack myStack;
    testStart("initStack");
    myStack.top = 0; // 適当な値にセット
    initStack(&myStack, N);
    assert_equals_int(myStack.top, N); // sp が最終値より後ろになっているか
    testEnd();
}
void testTopValue(void) {
    stack myStack;
    testStart("topValue");
    initStack(&myStack, N);
    /* stack が空なら NaN を返す */
    assert_equals_int(isnan(topValue(&myStack)), 1);
    /* stack が空でなければスタックのさしている数字を返す */
    myStack.top = 0;
    myStack.data[0] = 5.0;
    assert_equals_int(isnan(topValue(&myStack)), 0); // not NaN
    assert_equals_double(topValue(&myStack), 5.0);
    testEnd();
}
void push(void) {
    stack myStack;
    testStart("push");
    initStack(&myStack, N);
    push = 0.0
    push = 5.0
    push = 3.0
    assert_equals_int(isnan(push(&myStack)), 3.0);
    assert_equals_int(myStack.top, N-1);
    testEnd();
}

int main() {
    testInitStack();
    testTopValue();
    push();
    pull();
	return 0;
}
