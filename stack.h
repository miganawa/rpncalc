// stack.h by *****
#define N 10000

typedef struct {
    double data[N];
    int top;
} stack;

void initStack(stack *sp, int n);
double topValue(stack *sp);
void push(stack *stackp, double val);
double pull(stack *stackp);